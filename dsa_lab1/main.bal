import ballerina/io;
import ballerina/random;

public function main() returns error?{

    int randomNumber = check random:createIntInRange(10,60);

    io:println(randomNumber);

    foreach int i in 1...randomNumber {
        if (randomNumber % i == 0) {
            io:println(i);
        }
    }
    //oio:println("Hello, World!");
}
